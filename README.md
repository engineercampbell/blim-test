Please parse the JSON provided in the AssetDetailFragment. The JSON contains information about a show and its seasons/episodes. Retrieve and display the show title, synopsis, air date, and display it at the top of the fragment.

Right below this information parse the first season of the show and display each episode in a listview, display the episodes title and synopsis within the listview items.
 
Please make a button on the MainActivityFragment that will take you to the AssetDetailFragment

** The AssetDetailFragment contains a method that will parse the JSON to string. Feel free to ignore it if you wish to use a different technique.

** Feel free to use any libraries you wish to use.

** Don't be afraid to contact me with any questions.