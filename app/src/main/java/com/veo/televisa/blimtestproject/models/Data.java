
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("distributor")
    @Expose
    private Distributor distributor;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("genre")
    @Expose
    private Genre genre;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("keywords")
    @Expose
    private Object keywords;
    @SerializedName("modifiedAt")
    @Expose
    private String modifiedAt;
    @SerializedName("otherGenres")
    @Expose
    private List<OtherGenre> otherGenres = new ArrayList<OtherGenre>();
    @SerializedName("parentShow")
    @Expose
    private Object parentShow;
    @SerializedName("parentalRating")
    @Expose
    private ParentalRating parentalRating;
    @SerializedName("people")
    @Expose
    private List<Person> people = new ArrayList<Person>();
    @SerializedName("pictures")
    @Expose
    private List<Picture> pictures = new ArrayList<Picture>();
    @SerializedName("seasons")
    @Expose
    private List<Season> seasons = new ArrayList<Season>();
    @SerializedName("studio")
    @Expose
    private Object studio;
    @SerializedName("synopsis")
    @Expose
    private String synopsis;
    @SerializedName("synopsisShort")
    @Expose
    private String synopsisShort;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("titleSeo")
    @Expose
    private String titleSeo;
    @SerializedName("titleShort")
    @Expose
    private String titleShort;
    @SerializedName("videos")
    @Expose
    private List<Object> videos = new ArrayList<Object>();
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("airDate")
    @Expose
    private String airDate;
    @SerializedName("publicationEvent")
    @Expose
    private PublicationEvent publicationEvent;
    @SerializedName("averageRating")
    @Expose
    private Integer averageRating;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param studio
     * @param genre
     * @param titleSeo
     * @param averageRating
     * @param publicationEvent
     * @param keywords
     * @param modifiedAt
     * @param videos
     * @param entity
     * @param pictures
     * @param parentShow
     * @param people
     * @param airDate
     * @param id
     * @param category
     * @param title
     * @param duration
     * @param seasons
     * @param titleShort
     * @param synopsis
     * @param parentalRating
     * @param distributor
     * @param synopsisShort
     * @param otherGenres
     */
    public Data(Distributor distributor, Integer duration, String entity, Genre genre, Integer id, Object keywords, String modifiedAt, List<OtherGenre> otherGenres, Object parentShow, ParentalRating parentalRating, List<Person> people, List<Picture> pictures, List<Season> seasons, Object studio, String synopsis, String synopsisShort, String title, String titleSeo, String titleShort, List<Object> videos, String category, String airDate, PublicationEvent publicationEvent, Integer averageRating) {
        this.distributor = distributor;
        this.duration = duration;
        this.entity = entity;
        this.genre = genre;
        this.id = id;
        this.keywords = keywords;
        this.modifiedAt = modifiedAt;
        this.otherGenres = otherGenres;
        this.parentShow = parentShow;
        this.parentalRating = parentalRating;
        this.people = people;
        this.pictures = pictures;
        this.seasons = seasons;
        this.studio = studio;
        this.synopsis = synopsis;
        this.synopsisShort = synopsisShort;
        this.title = title;
        this.titleSeo = titleSeo;
        this.titleShort = titleShort;
        this.videos = videos;
        this.category = category;
        this.airDate = airDate;
        this.publicationEvent = publicationEvent;
        this.averageRating = averageRating;
    }

    /**
     * 
     * @return
     *     The distributor
     */
    public Distributor getDistributor() {
        return distributor;
    }

    /**
     * 
     * @param distributor
     *     The distributor
     */
    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    /**
     * 
     * @return
     *     The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * 
     * @param entity
     *     The entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * 
     * @return
     *     The genre
     */
    public Genre getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The keywords
     */
    public Object getKeywords() {
        return keywords;
    }

    /**
     * 
     * @param keywords
     *     The keywords
     */
    public void setKeywords(Object keywords) {
        this.keywords = keywords;
    }

    /**
     * 
     * @return
     *     The modifiedAt
     */
    public String getModifiedAt() {
        return modifiedAt;
    }

    /**
     * 
     * @param modifiedAt
     *     The modifiedAt
     */
    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    /**
     * 
     * @return
     *     The otherGenres
     */
    public List<OtherGenre> getOtherGenres() {
        return otherGenres;
    }

    /**
     * 
     * @param otherGenres
     *     The otherGenres
     */
    public void setOtherGenres(List<OtherGenre> otherGenres) {
        this.otherGenres = otherGenres;
    }

    /**
     * 
     * @return
     *     The parentShow
     */
    public Object getParentShow() {
        return parentShow;
    }

    /**
     * 
     * @param parentShow
     *     The parentShow
     */
    public void setParentShow(Object parentShow) {
        this.parentShow = parentShow;
    }

    /**
     * 
     * @return
     *     The parentalRating
     */
    public ParentalRating getParentalRating() {
        return parentalRating;
    }

    /**
     * 
     * @param parentalRating
     *     The parentalRating
     */
    public void setParentalRating(ParentalRating parentalRating) {
        this.parentalRating = parentalRating;
    }

    /**
     * 
     * @return
     *     The people
     */
    public List<Person> getPeople() {
        return people;
    }

    /**
     * 
     * @param people
     *     The people
     */
    public void setPeople(List<Person> people) {
        this.people = people;
    }

    /**
     * 
     * @return
     *     The pictures
     */
    public List<Picture> getPictures() {
        return pictures;
    }

    /**
     * 
     * @param pictures
     *     The pictures
     */
    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    /**
     * 
     * @return
     *     The seasons
     */
    public List<Season> getSeasons() {
        return seasons;
    }

    /**
     * 
     * @param seasons
     *     The seasons
     */
    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    /**
     * 
     * @return
     *     The studio
     */
    public Object getStudio() {
        return studio;
    }

    /**
     * 
     * @param studio
     *     The studio
     */
    public void setStudio(Object studio) {
        this.studio = studio;
    }

    /**
     * 
     * @return
     *     The synopsis
     */
    public String getSynopsis() {
        return synopsis;
    }

    /**
     * 
     * @param synopsis
     *     The synopsis
     */
    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    /**
     * 
     * @return
     *     The synopsisShort
     */
    public String getSynopsisShort() {
        return synopsisShort;
    }

    /**
     * 
     * @param synopsisShort
     *     The synopsisShort
     */
    public void setSynopsisShort(String synopsisShort) {
        this.synopsisShort = synopsisShort;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The titleSeo
     */
    public String getTitleSeo() {
        return titleSeo;
    }

    /**
     * 
     * @param titleSeo
     *     The titleSeo
     */
    public void setTitleSeo(String titleSeo) {
        this.titleSeo = titleSeo;
    }

    /**
     * 
     * @return
     *     The titleShort
     */
    public String getTitleShort() {
        return titleShort;
    }

    /**
     * 
     * @param titleShort
     *     The titleShort
     */
    public void setTitleShort(String titleShort) {
        this.titleShort = titleShort;
    }

    /**
     * 
     * @return
     *     The videos
     */
    public List<Object> getVideos() {
        return videos;
    }

    /**
     * 
     * @param videos
     *     The videos
     */
    public void setVideos(List<Object> videos) {
        this.videos = videos;
    }

    /**
     * 
     * @return
     *     The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The airDate
     */
    public String getAirDate() {
        return airDate;
    }

    /**
     * 
     * @param airDate
     *     The airDate
     */
    public void setAirDate(String airDate) {
        this.airDate = airDate;
    }

    /**
     * 
     * @return
     *     The publicationEvent
     */
    public PublicationEvent getPublicationEvent() {
        return publicationEvent;
    }

    /**
     * 
     * @param publicationEvent
     *     The publicationEvent
     */
    public void setPublicationEvent(PublicationEvent publicationEvent) {
        this.publicationEvent = publicationEvent;
    }

    /**
     * 
     * @return
     *     The averageRating
     */
    public Integer getAverageRating() {
        return averageRating;
    }

    /**
     * 
     * @param averageRating
     *     The averageRating
     */
    public void setAverageRating(Integer averageRating) {
        this.averageRating = averageRating;
    }

}
