
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Source {

    @SerializedName("headers")
    @Expose
    private Headers headers;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("messages")
    @Expose
    private List<Object> messages = new ArrayList<Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Source() {
    }

    /**
     * 
     * @param headers
     * @param data
     * @param messages
     */
    public Source(Headers headers, Data data, List<Object> messages) {
        this.headers = headers;
        this.data = data;
        this.messages = messages;
    }

    /**
     * 
     * @return
     *     The headers
     */
    public Headers getHeaders() {
        return headers;
    }

    /**
     * 
     * @param headers
     *     The headers
     */
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    /**
     * 
     * @return
     *     The data
     */
    public Data getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The messages
     */
    public List<Object> getMessages() {
        return messages;
    }

    /**
     * 
     * @param messages
     *     The messages
     */
    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

}
