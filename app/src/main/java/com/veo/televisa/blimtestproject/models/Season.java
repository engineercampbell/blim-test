
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Season {

    @SerializedName("airDate")
    @Expose
    private Object airDate;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("episodeCount")
    @Expose
    private Integer episodeCount;
    @SerializedName("episodes")
    @Expose
    private List<Episode> episodes = new ArrayList<Episode>();
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("title")
    @Expose
    private String title;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Season() {
    }

    /**
     * 
     * @param id
     * @param airDate
     * @param title
     * @param episodeCount
     * @param entity
     * @param number
     * @param episodes
     */
    public Season(Object airDate, String entity, Integer episodeCount, List<Episode> episodes, Integer id, Integer number, String title) {
        this.airDate = airDate;
        this.entity = entity;
        this.episodeCount = episodeCount;
        this.episodes = episodes;
        this.id = id;
        this.number = number;
        this.title = title;
    }

    /**
     * 
     * @return
     *     The airDate
     */
    public Object getAirDate() {
        return airDate;
    }

    /**
     * 
     * @param airDate
     *     The airDate
     */
    public void setAirDate(Object airDate) {
        this.airDate = airDate;
    }

    /**
     * 
     * @return
     *     The entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * 
     * @param entity
     *     The entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * 
     * @return
     *     The episodeCount
     */
    public Integer getEpisodeCount() {
        return episodeCount;
    }

    /**
     * 
     * @param episodeCount
     *     The episodeCount
     */
    public void setEpisodeCount(Integer episodeCount) {
        this.episodeCount = episodeCount;
    }

    /**
     * 
     * @return
     *     The episodes
     */
    public List<Episode> getEpisodes() {
        return episodes;
    }

    /**
     * 
     * @param episodes
     *     The episodes
     */
    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 
     * @param number
     *     The number
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

}
