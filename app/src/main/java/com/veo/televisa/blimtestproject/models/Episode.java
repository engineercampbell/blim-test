
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Episode {

    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("episodeNumber")
    @Expose
    private Integer episodeNumber;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pictures")
    @Expose
    private List<Object> pictures = new ArrayList<Object>();
    @SerializedName("synopsis")
    @Expose
    private String synopsis;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("category")
    @Expose
    private String category;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Episode() {
    }

    /**
     * 
     * @param id
     * @param category
     * @param title
     * @param duration
     * @param entity
     * @param synopsis
     * @param pictures
     * @param episodeNumber
     */
    public Episode(Integer duration, String entity, Integer episodeNumber, Integer id, List<Object> pictures, String synopsis, String title, String category) {
        this.duration = duration;
        this.entity = entity;
        this.episodeNumber = episodeNumber;
        this.id = id;
        this.pictures = pictures;
        this.synopsis = synopsis;
        this.title = title;
        this.category = category;
    }

    /**
     * 
     * @return
     *     The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * 
     * @param entity
     *     The entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * 
     * @return
     *     The episodeNumber
     */
    public Integer getEpisodeNumber() {
        return episodeNumber;
    }

    /**
     * 
     * @param episodeNumber
     *     The episodeNumber
     */
    public void setEpisodeNumber(Integer episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The pictures
     */
    public List<Object> getPictures() {
        return pictures;
    }

    /**
     * 
     * @param pictures
     *     The pictures
     */
    public void setPictures(List<Object> pictures) {
        this.pictures = pictures;
    }

    /**
     * 
     * @return
     *     The synopsis
     */
    public String getSynopsis() {
        return synopsis;
    }

    /**
     * 
     * @param synopsis
     *     The synopsis
     */
    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

}
