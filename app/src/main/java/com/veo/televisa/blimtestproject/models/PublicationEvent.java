
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PublicationEvent {

    @SerializedName("billingType")
    @Expose
    private String billingType;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("startDate")
    @Expose
    private String startDate;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PublicationEvent() {
    }

    /**
     * 
     * @param id
     * @param startDate
     * @param price
     * @param entity
     * @param endDate
     * @param billingType
     */
    public PublicationEvent(String billingType, String endDate, String entity, Integer id, String price, String startDate) {
        this.billingType = billingType;
        this.endDate = endDate;
        this.entity = entity;
        this.id = id;
        this.price = price;
        this.startDate = startDate;
    }

    /**
     * 
     * @return
     *     The billingType
     */
    public String getBillingType() {
        return billingType;
    }

    /**
     * 
     * @param billingType
     *     The billingType
     */
    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    /**
     * 
     * @return
     *     The endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * 
     * @param endDate
     *     The endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * 
     * @return
     *     The entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * 
     * @param entity
     *     The entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * 
     * @param startDate
     *     The startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}
