
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class OtherGenre {

    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("items")
    @Expose
    private List<Object> items = new ArrayList<Object>();
    @SerializedName("parent")
    @Expose
    private Object parent;
    @SerializedName("title")
    @Expose
    private String title;

    /**
     * No args constructor for use in serialization
     * 
     */
    public OtherGenre() {
    }

    /**
     * 
     * @param id
     * @param title
     * @param items
     * @param entity
     * @param parent
     */
    public OtherGenre(String entity, Integer id, List<Object> items, Object parent, String title) {
        this.entity = entity;
        this.id = id;
        this.items = items;
        this.parent = parent;
        this.title = title;
    }

    /**
     * 
     * @return
     *     The entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * 
     * @param entity
     *     The entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The items
     */
    public List<Object> getItems() {
        return items;
    }

    /**
     * 
     * @param items
     *     The items
     */
    public void setItems(List<Object> items) {
        this.items = items;
    }

    /**
     * 
     * @return
     *     The parent
     */
    public Object getParent() {
        return parent;
    }

    /**
     * 
     * @param parent
     *     The parent
     */
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

}
