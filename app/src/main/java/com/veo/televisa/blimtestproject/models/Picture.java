
package com.veo.televisa.blimtestproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Picture {

    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pictureType")
    @Expose
    private String pictureType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("versions")
    @Expose
    private List<Version> versions = new ArrayList<Version>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Picture() {
    }

    /**
     * 
     * @param id
     * @param versions
     * @param title
     * @param pictureType
     * @param entity
     */
    public Picture(String entity, Integer id, String pictureType, String title, List<Version> versions) {
        this.entity = entity;
        this.id = id;
        this.pictureType = pictureType;
        this.title = title;
        this.versions = versions;
    }

    /**
     * 
     * @return
     *     The entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * 
     * @param entity
     *     The entity
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The pictureType
     */
    public String getPictureType() {
        return pictureType;
    }

    /**
     * 
     * @param pictureType
     *     The pictureType
     */
    public void setPictureType(String pictureType) {
        this.pictureType = pictureType;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The versions
     */
    public List<Version> getVersions() {
        return versions;
    }

    /**
     * 
     * @param versions
     *     The versions
     */
    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

}
